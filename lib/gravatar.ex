defmodule SampleApp.Gravatar do
  # Gravatarから画像を取得する関数
  def get_gravatar_url(email, size) do
    gravatar_id = email_to_gravatar_id(email)
    "https://secure.gravatar.com/avatar/#{gravatar_id}?s=#{size}"
  end

  # EmailからIDに変換する関数
  defp email_to_gravatar_id(email) do
    email |> email_downcase |> email_crypt_md5
  end

  # Emailを暗号化する関数
  # キャプチャ構文による匿名関数の定義に注意
  # 処理がわからない場合はiex上で実行することで確認可能
  defp email_crypt_md5(email) do
    :erlang.md5(email)
    |> :erlang.bitstring_to_list
    |> Enum.map(&(:io_lib.format("~2.16.0b", [&1])))
    |> List.flatten
    |> :erlang.list_to_bitstring
  end

  # 文字列をすべて小文字に変換
  defp email_downcase(email) do
    String.downcase(email)
  end

end
