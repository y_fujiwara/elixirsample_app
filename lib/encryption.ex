# 暗号化用モジュール 暗号化/復号化にはSafetyboxを利用
defmodule SampleApp.Encryption do
  def decrypt(password) do
    Safetybox.decrypt(password)
  end
  def encrypt(password) do
    Safetybox.encrypt(password, :default)
end end
