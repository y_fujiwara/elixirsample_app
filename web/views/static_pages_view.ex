defmodule SampleApp.StaticPagesView do
  # 静的ページテンプレート用ビュー
  # ここで定義された関数はテンプレートから使える
  use SampleApp.Web, :view
end
