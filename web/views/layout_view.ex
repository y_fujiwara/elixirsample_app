defmodule SampleApp.LayoutView do
  use SampleApp.Web, :view

  # デバッグ情報としてコントローラー名とアクション名を表示する関数
  def get_controller_name(conn), do: controller_module(conn)
  def get_action_name(conn), do: action_name(conn)
end
