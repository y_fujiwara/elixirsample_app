defmodule SampleApp.StaticPagesController do
  use SampleApp.Web, :controller
  # アクションを関数として定義

  # messageを変数としてテンプレート内でつかえるように
  def home(conn, _params) do
    render conn, "home.html"
  end

  def help(conn, _params) do
    render conn, "help.html", message: "Help"
  end

  def about(conn, _params) do
    render conn, "about.html", message: "About"
  end

  def contact(conn, _params) do
    render conn, "contact.html"
  end
end
