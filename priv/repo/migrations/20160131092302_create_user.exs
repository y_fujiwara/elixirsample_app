defmodule SampleApp.Repo.Migrations.CreateUser do
  use Ecto.Migration
  # トランザクションの外側で実行するように強制
  @disable_ddl_transaction true
  # create tableでテーブル追加(createはやっぱりマクロらしい)
  # addで絡む追加
  def change do
    create table(:users) do
      add :name, :string
      add :email, :string

      timestamps
    end

    create index(:users, [:name], unique: true, concurrently: true)
    create index(:users, [:email], unique: true, concurrently: true)
  end
end
